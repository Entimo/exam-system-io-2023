#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
    FILE *inputFile, *outputFile;

    outputFile = fopen("../samples/output.txt", "w");
    if (outputFile == NULL) {
        printf("Impossible de créer le fichier de sortie.\n");
        return 1;
    }

    if (argc == 1) {
       
        int c;
        while ((c = getchar()) != EOF) {
            fputc(c, outputFile);
        }
    } else {
     
        for (int i = 1; i < argc; i++) {
            inputFile = fopen(argv[i], "r+");
            if (inputFile == NULL) {
                printf("Impossible d'ouvrir le fichier %s\n", argv[i]);
                return 1;
            }

            char line[256];
            while (fgets(line, sizeof(line), inputFile) != NULL) {
                fputs(line, outputFile);
            }

            fclose(inputFile);
            
        }

    }
    fclose(outputFile); 
    return 0;
}
